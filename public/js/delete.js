$('#confirm-delete').on('show.bs.modal', function (e) {
  var target = $(e.relatedTarget);
  $('.optional-message', this).text(target.data('text'));
  $('form', this).attr('action', target.data('url'));
});
