@extends('layouts.app')

@section('title', trans('navigation.top.login'))

@section('content')

  @component('forms.form', ['action' => route('login')])
    @include('forms.group', ['type' => 'email', 'name' => 'email', 'label' => trans('labels.email')])
    @include('forms.group', ['type' => 'password', 'name' => 'password', 'label' => trans('labels.password')])
    @include('forms.group', ['type' => 'checkbox', 'name' => 'remember', 'label' => trans('labels.remember_me')])

    @component('forms.submit')
      @lang('buttons.login')
    @endcomponent
  @endcomponent

@endsection
