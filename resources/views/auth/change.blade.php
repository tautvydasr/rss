@extends('layouts.app')

@section('title', trans('navigation.top.change_password'))

@section('content')

  @component('forms.form')
    @include('forms.group', ['type' => 'password', 'name' => 'password', 'label' => trans('labels.new_password')])
    @include('forms.group', ['type' => 'password', 'name' => 'password_confirmation', 'label' => trans('labels.confirm_password')])

    @component('forms.submit')
      @lang('buttons.change_password')
    @endcomponent
  @endcomponent

@endsection
