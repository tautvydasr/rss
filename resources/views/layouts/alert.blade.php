@foreach(['success', 'error'] as $flash)
  @if (session()->has($flash))
    <div class="alert alert-{{ $flash }} alert-dismissable" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="@lang('buttons.close')">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong>{{ ucfirst($flash) }}!</strong> {{ session($flash) }}
    </div>
  @endif
@endforeach
