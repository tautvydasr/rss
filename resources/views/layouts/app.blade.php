<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div id="app">
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                  data-target="#app-navbar-collapse" aria-expanded="false">
            <span class="sr-only">@lang('navigation.toggle')</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <a class="navbar-brand" href="{{ route('feeds.index') }}">
            {{ config('app.name', 'Laravel') }}
          </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
          @include('navigation.top')
        </div>
      </div>
    </nav>

    @php($isSidebarVisible = auth()->check() && auth()->user()->isAdmin())
    <div class="container">
      @include('layouts.alert')

      <div class="row">
        @if ($isSidebarVisible)
          <div class="col-md-3">
            @include('navigation.sidebar')
          </div>
        @endif

        <div class="col-md-{{ $isSidebarVisible ? '9' : '10 col-md-offset-1' }}">
          <div class="panel panel-default">

            @hasSection('title')
              <div class="panel-heading">
                @yield('title')
                @yield('actions')
              </div>
            @endif

            <div class="@yield('content-class', 'panel-body')">
              @yield('content')
            </div>
          </div>
        </div>
      </div>
      @hasSection('pagination')
        <div class="row">
          <div class="col-md-{{ $isSidebarVisible ? 9 : 10 }} col-md-offset-{{ $isSidebarVisible ? 3 : 1 }} text-center">
            @yield('pagination')
          </div>
        </div>
      @endif
    </div>
  </div>

  <script src="{{ asset('js/app.js') }}"></script>
  @stack('scripts')
</body>
</html>
