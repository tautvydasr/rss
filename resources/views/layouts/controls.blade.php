@if (isset($edit))
  <a href="{{ $edit }}" class="btn btn-xs btn-info">@lang('buttons.edit')</a>
@endif

@if (isset($delete))
  <a href="#" class="btn btn-xs btn-danger" data-url="{{ $delete }}" data-text="{{ $message ?? null }}"
     data-toggle="modal" data-target="#confirm-delete">@lang('buttons.delete')</a>
@endif
