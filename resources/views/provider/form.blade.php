@include('forms.group', [
  'name' => 'title',
  'label' => trans('labels.title'),
  'value' => $provider->title ?? null
])
@include('forms.group', [
  'type' => 'url',
  'name' => 'url',
  'label' => trans('labels.url'),
  'value' => $provider->url ?? null
])
@include('forms.group', [
  'type' => 'select',
  'name' => 'category_id',
  'label' => trans('labels.category'),
  'options' => $categories,
  'placeholder' => trans('placeholders.select.category'),
  'value' => $provider->category->id ?? null,
])
