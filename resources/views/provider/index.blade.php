@extends('layouts.app')

@section('title', trans('sections.provider.index'))

@section('actions')
  <a href="{{ route('providers.create') }}"
     class="btn btn-xs btn-success pull-right">@lang('buttons.create')</a>
@endsection

@section('content-class', 'table-responsive')

@section('content')
  <table class="table table-bordered">
    <tr>
      <th>#</th>
      <th>@lang('labels.title')</th>
      <th>@lang('labels.url')</th>
      <th></th>
    </tr>
    @forelse ($groupedProviders as $group => $providers)
      <tr class="warning">
        <td colspan="4">
          <strong>{{ $group ?: trans('labels.uncategorized') }}</strong>
        </td>
      </tr>
      @each('provider.row', $providers, 'provider')
    @empty
      @include('layouts.empty', ['cols' => 4])
    @endforelse
  </table>
@endsection

@push('scripts')
  @include('modals.delete')
  <script src="{{ asset('js/delete.js') }}"></script>
@endpush
