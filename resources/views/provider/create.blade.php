@extends('layouts.app')

@section('title', trans('sections.provider.index'))

@section('content')

  @component('forms.form', ['action' => route('providers.store')])
    @include('provider.form')

    @component('forms.submit')
      @lang('buttons.create')
    @endcomponent
  @endcomponent

@endsection
