@extends('layouts.app')

@section('title', trans('sections.provider.update'))

@section('content')

  @component('forms.form', ['method' => 'PATCH', 'action' => route('providers.update', ['provider' => $provider->id])])
    @include('provider.form')

    @component('forms.submit')
      @lang('buttons.update')
    @endcomponent
  @endcomponent

@endsection
