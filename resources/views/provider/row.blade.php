<tr>
  <td>{{ $provider->id }}</td>
  <td>{{ $provider->title }}</td>
  <td>
    <a href="{{ $provider->url }}" target="_blank">
      {{ $provider->url }}
    </a>
  </td>
  <td nowrap>
    @php($feedCount = $provider->feeds->count())
    @include('layouts.controls', [
      'edit' => route('providers.edit', ['provider' => $provider->id]),
      'delete' => route('providers.destroy', ['provider' => $provider->id]),
      'message' => $feedCount > 0 ? trans_choice('messages.feed_delete', $feedCount, ['count' => $feedCount]) : null,
    ])
  </td>
</tr>
