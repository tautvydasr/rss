<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    @component('forms.form', ['method' => 'DELETE'])
      <div class="modal-content">
        <div class="modal-body">
          <span class="optional-message"></span>@lang('messages.confirm_delete')
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-sm">@lang('buttons.confirm')</button>
          <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">@lang('buttons.cancel')</button>
        </div>
      </div>
    @endcomponent
  </div>
</div>
