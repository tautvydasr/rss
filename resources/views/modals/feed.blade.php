<div class="modal fade" id="feed{{ $feed->id }}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="@lang('buttons.close')">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">
          {{ $feed->title }}
        </h4>
      </div>
      <div class="modal-body">
        @if ($feed->description)
          {{ $feed->description }}
        @else
          <i>@lang('messages.no_description')</i>
        @endif
      </div>
      <div class="modal-footer">
        <a href="{{ $feed->link }}" class="btn btn-primary" target="_blank">@lang('buttons.preview')</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('buttons.close')</button>
      </div>
    </div>
  </div>
</div>
