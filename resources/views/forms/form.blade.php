@php($method = $method ?? 'POST')
<form class="form-horizontal"
      method="{{ strtoupper($method) === 'GET' ? 'GET' : 'POST' }}"
      action="{{ $action ?? '' }}"
      novalidate>
  @if (in_array(strtoupper($method), ['PUT', 'PATCH', 'DELETE']))
    {{ method_field($method) }}
  @endif
  {{ csrf_field() }}
  {{ $slot }}
</form>
