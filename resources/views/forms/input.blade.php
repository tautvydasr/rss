<input id="{{ $name }}"
       type="{{ $type ?? 'text' }}"
       class="form-control"
       name="{{ $name }}"
       value="{{ old($name, $value ?? null) }}">
