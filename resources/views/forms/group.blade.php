@php($type = $type ?? 'text')
@php($label = $label ?? ucfirst($name))
@php($offsetInput = in_array($type, ['checkbox']))

<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
  @includeWhen(!$offsetInput, 'forms.label')

  <div class="col-md-6{{ $offsetInput ? ' col-md-offset-4' : '' }}">
    @switch($type)
      @case('select')
      @case('checkbox')
        @include("forms.$type")
        @break

      @default
        @include('forms.input')
    @endswitch

    @include('forms.error')
  </div>
</div>
