<div class="checkbox">
  <label>
    <input type="checkbox" name="{{ $name }}" {{ old($name) ? 'checked' : '' }}> {{ $label }}
  </label>
</div>
