<select id="{{ $name }}" class="form-control" name="{{ $name }}">
  @if (isset($placeholder))
    <option value="">{{ $placeholder }}</option>
  @endif
  @foreach ($options as $val => $title)
    <option value="{{ $val }}"{{ $val == old($name, $value ?? null) ? ' selected' : '' }}>
      {{ $title }}
    </option>
  @endforeach
</select>
