@extends('layouts.app')

@section('title', '&nbsp;')

@section('actions')
  <select class="form-control input-sm pull-right"
          title="@lang('labels.filter_by_category')"
          onchange="document.location.href=this.value;"
          style="max-width: 250px;"{{ $categories->isEmpty() ? ' disabled' : '' }}>
    <option value="{{ route('feeds.index') }}"{{ $selectedCategory === null ?  ' selected' : '' }}>
      @lang('placeholders.select.category')
    </option>
    @foreach ($categories as $category)
      @php($isSelected = $selectedCategory !== null && request('category') === $category)
      <option value="{{ route('feeds.index', ['category' => $category ?? '']) }}"{{ $isSelected ? ' selected' : '' }}>
        {{ $category ?? trans('labels.uncategorized') }}
      </option>
    @endforeach
  </select>
  <div class="clearfix"></div>
@endsection

@section('content-class', 'table-responsive')

@section('content')
  <table class="table table-bordered">
    <tr>
      <th>@lang('labels.title')</th>
      <th>@lang('labels.category')</th>
      <th>@lang('labels.provider')</th>
      <th>@lang('labels.published_at')</th>
    </tr>
    @each('feed.row', $feeds, 'feed', 'layouts.empty')
  </table>
@endsection

@section('pagination')
  {{ $feeds->appends(['category' => $selectedCategory])->links() }}
@endsection
