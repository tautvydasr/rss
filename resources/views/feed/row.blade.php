<tr>
  <td>
    <a href="#" data-toggle="modal" data-target="#feed{{ $feed->id }}">
      {{ $feed->title }}
    </a>
    @include('modals.feed')
  </td>
  <td>
    <a href="{{ route('feeds.index', ['category' => $feed->category ?? '']) }}">
      {{ $feed->category ?? trans('labels.uncategorized') }}
    </a>
  </td>
  <td>
    <a href="{{ $feed->provider->url }}" target="_blank">
      {{ $feed->provider->title }}
    </a>
  </td>
  <td nowrap>{{ $feed->published_at->format('Y-m-d H:i') }}</td>
</tr>
