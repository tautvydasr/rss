@extends('layouts.app')

@section('title', trans('sections.category.index'))

@section('content')

  @component('forms.form', ['action' => route('categories.store')])
    @include('forms.group', ['name' => 'title', 'label' => trans('labels.title')])

    @component('forms.submit')
      @lang('buttons.create')
    @endcomponent
  @endcomponent

@endsection
