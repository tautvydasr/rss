@extends('layouts.app')

@section('title', trans('sections.category.index'))

@section('actions')
  <a href="{{ route('categories.create') }}"
     class="btn btn-xs btn-success pull-right">@lang('buttons.create')</a>
@endsection

@section('content-class', 'table-responsive')

@section('content')
  <table class="table table-bordered">
    <tr>
      <th>#</th>
      <th>@lang('labels.title')</th>
      <th></th>
    </tr>
    @each('category.row', $categories, 'category', 'layouts.empty')
  </table>
@endsection

@push('scripts')
  @include('modals.delete')
  <script src="{{ asset('js/delete.js') }}"></script>
@endpush
