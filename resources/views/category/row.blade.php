<tr>
  <td>{{ $category->id }}</td>
  <td>{{ $category->title }}</td>
  <td nowrap>
    @include('layouts.controls', [
      'edit' => route('categories.edit', ['category' => $category->id]),
      'delete' => route('categories.destroy', ['category' => $category->id]),
    ])
  </td>
</tr>
