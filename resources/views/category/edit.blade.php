@extends('layouts.app')

@section('title', trans('sections.category.update'))

@section('content')

  @component('forms.form', ['method' => 'PATCH', 'action' => route('categories.update', ['category' => $category->id])])
    @include('forms.group', ['name' => 'title', 'label' => trans('labels.title'), 'value' => $category->title])

    @component('forms.submit')
      @lang('buttons.update')
    @endcomponent
  @endcomponent

@endsection
