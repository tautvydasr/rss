<div class="list-group">
  @foreach (trans('navigation.sidebar') as $key => $section)
    <a href="{{ route("$key.index") }}"
       class="list-group-item{{ request()->routeIs("$key.*") ? ' active' : '' }}">{{ $section }}</a>
  @endforeach
</div>
