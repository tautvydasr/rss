<?php

return [
    'model' => [
        'feed' => 'Feed',
        'category' => 'Category',
        'provider' => 'Provider',
        'password' => 'Password',
    ],
    'email' => 'Email',
    'password' => 'Password',
    'new_password' => 'New password',
    'confirm_password' => 'Confirm password',
    'remember_me' => 'Remember me',
    'title' => 'Title',
    'category' => 'Category',
    'provider' => 'Provider',
    'published_at' => 'Published at',
    'uncategorized' => 'Uncategorized',
    'url' => 'Url',
    'filter_by_category' => 'Filter feed list by category',
    'empty' => 'No entries found',
];
