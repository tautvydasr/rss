<?php

return [
    'confirm_delete' => 'Are you sure you want to delete this item?',
    'no_description' => 'No description provided',
    'feed_delete' => 'There are :count feed related with this provider, they will be deleted too. |There are :count feeds related with this provider, they will be deleted too. ',
    'flash' => [
        'changed' => ':model has been changed.',
        'created' => ':model has been created.',
        'updated' => ':model has been updated.',
        'deleted' => ':model has been deleted.',
    ],
];