<?php

return [
    'change_password' => 'Change password',
    'login' => 'Login',
    'create' => 'Create',
    'update' => 'Update',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'confirm' => 'Confirm',
    'cancel' => 'Cancel',
    'preview' => 'Preview',
    'close' => 'Close',
];
