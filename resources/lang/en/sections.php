<?php

return [
    'category' => [
        'index' => 'Categories',
        'create' => 'Create category',
        'update' => 'Update category',
    ],
    'provider' => [
        'index' => 'Providers',
        'create' => 'Create provider',
        'update' => 'Update provider',
    ],
];
