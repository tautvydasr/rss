<?php

return [
    'toggle' => 'Toggle Navigation',
    'top' => [
        'change_password' => 'Change password',
        'login' => 'Login',
        'logout' => 'Logout',
    ],
    'sidebar' => [
        'feeds' => 'Feeds',
        'categories' => 'Categories',
        'providers' => 'Providers',
    ],
];
