<?php

Route::group(['middleware' => 'guest'], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
});

Route::group(['middleware' => 'auth'], function () {
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    Route::group(['prefix' => 'change-password'], function () {
        Route::get('', 'Auth\ChangePasswordController@showChangePasswordForm')->name('password.change');
        Route::post('', 'Auth\ChangePasswordController@changePassword');
    });

    Route::group(['middleware' => 'admin'], function () {
        Route::resource('providers', 'ProviderController', ['except' => 'show']);
        Route::resource('categories', 'CategoryController', ['except' => 'show']);
    });
});

Route::get('/', 'FeedController@index')->name('feeds.index');
