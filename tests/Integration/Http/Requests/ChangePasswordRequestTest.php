<?php

namespace Tests\Integration\Http\Requests;

use App\Http\Requests\ChangePasswordRequest;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;

class ChangePasswordRequestTest extends RequestTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User();
    }

    public function testFailedAuthorization(): void
    {
        $this->user = null;
        $this->expectException(AuthorizationException::class);
        $this->app->make($this->getRequestClass());
    }

    public function testPasswordIsRequired(): void
    {
        $this->createRequest(['password' => null]);
        $this->assertValidationError('password', 'The password field is required.');
    }

    public function testPasswordIsAtLeastSixChars(): void
    {
        $this->createRequest(['password' => 'short']);
        $this->assertValidationError('password', 'The password must be at least 6 characters.');
    }

    public function testPasswordIsConfirmed(): void
    {
        $this->createRequest(['password' => 'yes', 'password_confirmation' => 'no']);
        $this->assertValidationError('password', 'The password confirmation does not match.');
    }

    public function testValidRequest(): void
    {
        $this->createRequest(['password' => 'very-valid', 'password_confirmation' => 'very-valid']);
        $this->assertValidRequest();
    }

    protected function getRequestClass(): string
    {
        return ChangePasswordRequest::class;
    }
}