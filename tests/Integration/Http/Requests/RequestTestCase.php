<?php

namespace Tests\Integration\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\ParameterBag;
use Tests\TestCase;

abstract class RequestTestCase extends TestCase
{
    /**
     * @var User
     */
    protected $user;

    /**
     * @var Route
     */
    protected $route;

    abstract protected function getRequestClass(): string;

    protected function createRequest(array $inputs): void
    {
        /** @var \Illuminate\Http\Request $request */
        $request = $this->app['request'];
        $request->query = new ParameterBag($inputs);
        $request->setUserResolver($this->getUserResolver());
        $request->setRouteResolver($this->getRouteResolver());
        $this->app['request'] = $request;
    }

    protected function getUserResolver(): \Closure
    {
        return function () {
            return $this->user;
        };
    }

    protected function getRouteResolver(): \Closure
    {
        return function () {
            return $this->route;
        };
    }

    protected function assertValidationError(string $key, string $error): void
    {
        try {
            $this->expectException(ValidationException::class);
            $this->app->make($this->getRequestClass());
        } catch (ValidationException $e) {
            $this->assertContains($error, array_get($e->errors(), $key, []));
            throw $e;
        }
    }

    protected function assertValidRequest(): void
    {
        try {
            $this->assertInstanceof(FormRequest::class, $this->app->make($this->getRequestClass()));
        } catch (ValidationException $e) {
            $this->fail('Expected valid request, got ValidationException: ' . $e->getMessage());
        }
    }
}