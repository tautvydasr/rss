<?php

namespace Tests\Integration\Http\Requests;

use App\Http\Requests\ProviderRequest;
use App\Models\Category;
use App\Models\Provider;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Routing\Route;

class ProviderRequestTest extends RequestTestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User(['role' => User::ROLE_ADMIN]);
    }

    public function testFailedAuthorization(): void
    {
        $this->user = null;
        $this->expectException(AuthorizationException::class);
        $this->app->make($this->getRequestClass());
    }

    public function testTitleIsRequired(): void
    {
        $this->createRequest(['title' => null]);
        $this->assertValidationError('title', 'The title field is required.');
    }

    public function testTitleIsString(): void
    {
        $this->createRequest(['title' => 666]);
        $this->assertValidationError('title', 'The title must be a string.');
    }

    public function testTitleIsUnique(): void
    {
        $provider = factory(Provider::class)->create();
        $this->createRequest(['title' => $provider->title]);
        $this->assertValidationError('title', 'The title has already been taken.');
    }

    public function testUrlIsRequired(): void
    {
        $this->createRequest(['url' => null]);
        $this->assertValidationError('url', 'The url field is required.');
    }

    public function testUrlIsString(): void
    {
        $this->createRequest(['url' => 55]);
        $this->assertValidationError('url', 'The url must be a string.');
    }

    public function testUrlIsUnique(): void
    {
        $provider = factory(Provider::class)->create();
        $this->createRequest(['url' => $provider->url]);
        $this->assertValidationError('url', 'The url has already been taken.');
    }

    public function testUrlFormatIsCorrect(): void
    {
        $this->createRequest(['url' => 'incorrect-format']);
        $this->assertValidationError('url', 'The url format is invalid.');
    }

    public function testCategoryIsInteger(): void
    {
        $this->createRequest(['category_id' => 'string']);
        $this->assertValidationError('category_id', 'The category id must be an integer.');
    }

    public function testCategoryExistsOnDatabase(): void
    {
        $this->createRequest(['category_id' => 99999]);
        $this->assertValidationError('category_id', 'The selected category id is invalid.');
    }

    public function testValidRequestWithEmptyCategory(): void
    {
        $this->createRequest(['title' => 'Centurion', 'url' => 'https://www.centurion.net', 'category_id' => null]);
        $this->assertValidRequest();
    }

    public function testValidRequestIgnoringExistingProvider(): void
    {
        $provider = factory(Provider::class)->create();
        $route = $this->prophesize(Route::class);
        $route->parameter('provider')->shouldBeCalled()->willReturn($provider);
        $this->route = $route->reveal();

        $this->createRequest($provider->getAttributes());
        $this->assertValidRequest();
    }

    public function testValidRequest(): void
    {
        $category = factory(Category::class)->create();
        $this->createRequest(['title' => 'BBC', 'url' => 'https://www.bbc.co.uk', 'category_id' => $category->id]);
        $this->assertValidRequest();
    }

    protected function getRequestClass(): string
    {
        return ProviderRequest::class;
    }
}