<?php

namespace Tests\Integration\Http\Requests;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Routing\Route;

class CategoryRequestTest extends RequestTestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User(['role' => User::ROLE_ADMIN]);
    }

    public function testFailedAuthorization(): void
    {
        $this->user = null;
        $this->expectException(AuthorizationException::class);
        $this->app->make($this->getRequestClass());
    }

    public function testTitleIsRequired(): void
    {
        $this->createRequest(['title' => null]);
        $this->assertValidationError('title', 'The title field is required.');
    }

    public function testTitleIsString(): void
    {
        $this->createRequest(['title' => 22]);
        $this->assertValidationError('title', 'The title must be a string.');
    }

    public function testTitleIsUnique(): void
    {
        $category = factory(Category::class)->create();
        $this->createRequest(['title' => $category->title]);
        $this->assertValidationError('title', 'The title has already been taken.');
    }

    public function testValidRequest(): void
    {
        $this->createRequest(['title' => 'Very valid title']);
        $this->assertValidRequest();
    }

    public function testValidRequestIgnoringExistingCategory(): void
    {
        $category = factory(Category::class)->create();
        $route = $this->prophesize(Route::class);
        $route->parameter('category')->shouldBeCalled()->willReturn($category);
        $this->route = $route->reveal();

        $this->createRequest(['title' => $category->title]);
        $this->assertValidRequest();
    }

    protected function getRequestClass(): string
    {
        return CategoryRequest::class;
    }
}