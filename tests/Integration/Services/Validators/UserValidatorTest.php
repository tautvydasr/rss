<?php

namespace Tests\Integration\Services\Validators;

use App\Models\User;
use App\Services\Validators\UserValidator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use Tests\TestCase;

class UserValidatorTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * @var UserValidator
     */
    private $validator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->validator = $this->app->make(UserValidator::class);
    }

    public function testEmailFieldIsRequired(): void
    {
        $this->assertContains('The email field is required.', $this->getErrors(['email' => null]));
    }

    public function testEmailFieldIsValidEmail(): void
    {
        $this->assertContains(
            'The email must be a valid email address.',
            $this->getErrors(['email' => 'invalid-email'])
        );
    }

    public function testEmailFieldIsUnique(): void
    {
        $user = factory(User::class)->create();

        $this->assertContains(
            'The email has already been taken.',
            $this->getErrors(['email' => $user->email])
        );
    }

    public function testPasswordFieldIsRequired(): void
    {
        $this->assertContains(
            'The password field is required.',
            $this->getErrors(['password' => null])
        );
    }

    public function testPasswordFieldHasAtLeastSixChars(): void
    {
        $this->assertContains(
            'The password must be at least 6 characters.',
            $this->getErrors(['password' => 'short'])
        );
    }

    public function testPasswordFieldIsConfirmed(): void
    {
        $this->assertContains(
            'The password confirmation does not match.',
            $this->getErrors(['password' => 'yes', 'password_confirmation' => 'no'])
        );
    }

    public function testRoleIsRequired(): void
    {
        $this->assertContains(
            'The role field is required.',
            $this->getErrors(['role' => null])
        );
    }

    public function testRoleIsValid(): void
    {
        $this->assertContains(
            'The selected role is invalid.',
            $this->getErrors(['role' => 'wizard'])
        );
    }

    public function testValidFields(): void
    {
        $errors = $this-> getErrors([
            'email' => 'valid@email.lt',
            'password' => 'secret',
            'password_confirmation' => 'secret',
            'role' => User::ROLE_ADMIN,
        ]);

        $this->assertEmpty($errors);
    }

    private function getErrors(array $inputToValidate): Collection
    {
        return $this->validator->validate($inputToValidate);
    }
}