<?php

namespace Tests\Integration\Services\Validators;

use App\Models\Feed;
use App\Services\Validators\FeedValidator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use Tests\TestCase;

class FeedValidatorTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * @var FeedValidator
     */
    private $validator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->validator = $this->app->make(FeedValidator::class);
    }

    public function testGuidIsRequired(): void
    {
        $this->assertContains(
            'The guid field is required.',
            $this->getErrors(['guid' => null])
        );
    }

    public function testGuidIsUnique(): void
    {
        $feed = factory(Feed::class)->create();
        $this->assertContains(
            'The guid has already been taken.',
            $this->getErrors(['guid' => $feed->guid])
        );
    }

    public function testProviderIsRequired(): void
    {
        $this->assertContains(
            'The provider id field is required.',
            $this->getErrors(['provider_id' => null])
        );
    }

    public function testProviderIsInteger(): void
    {
        $this->assertContains(
            'The provider id must be an integer.',
            $this->getErrors(['provider_id' => 'not-integer'])
        );
    }

    public function testProviderExistsOnDatabase(): void
    {
        $this->assertContains(
            'The selected provider id is invalid.',
            $this->getErrors(['provider_id' => 99999])
        );
    }

    public function testTitleIsRequired(): void
    {
        $this->assertContains(
            'The title field is required.',
            $this->getErrors(['title' => null])
        );
    }

    public function testTitleIsString(): void
    {
        $this->assertContains(
            'The title must be a string.',
            $this->getErrors(['title' => 89])
        );
    }

    public function testLinkIsRequired(): void
    {
        $this->assertContains(
            'The link field is required.',
            $this->getErrors(['link' => null])
        );
    }

    public function testLinkIsValidUrl(): void
    {
        $this->assertContains(
            'The link format is invalid.',
            $this->getErrors(['link' => 'invalid-url'])
        );
    }

    public function testDescriptionIsString(): void
    {
        $this->assertContains(
            'The description must be a string.',
            $this->getErrors(['description' => 7])
        );
    }

    public function testCategoryIsString(): void
    {
        $this->assertContains(
            'The category must be a string.',
            $this->getErrors(['category' => 66])
        );
    }

    public function testPublishedAtIsDate(): void
    {
        $this->assertContains(
            'The published at is not a valid date.',
            $this->getErrors(['published_at' => 'invalid-date'])
        );
    }

    public function testValidWithNullableFields(): void
    {
        $feed = factory(Feed::class)->make(['description' => null, 'category' => null]);
        $this->assertEmpty($this->getErrors($feed->getAttributes()));
    }

    public function testValidFields(): void
    {
        $feed = factory(Feed::class)->make();
        $this->assertEmpty($this->getErrors($feed->getAttributes()));
    }

    private function getErrors(array $inputToValidate): Collection
    {
        return $this->validator->validate($inputToValidate);
    }
}