<?php

namespace Tests\Integration\Repositories\Eloquent;

use App\Models\Feed;
use App\Repositories\Eloquent\FeedRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class FeedRepositoryTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * @var FeedRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(FeedRepository::class);
    }

    public function testFindPaginatedByCategoryWithEmptyCategory(): void
    {
        $expectedFeeds = factory(Feed::class, 2)->create();
        $pagination = $this->repository->findPaginatedByCategory();

        $expectedFeeds->each(function (Feed $expected) use ($pagination) {
            $this->assertContains($expected->guid, array_pluck($pagination->items(), 'guid'));
        });
    }

    public function testFindPaginatedByCategoryWithUncatgorized(): void
    {
        $feedWithCategory = factory(Feed::class)->create(['category' => 'Laravel']);
        $feedWithoutCategory = factory(Feed::class)->create(['category' => null]);
        $pagination = $this->repository->findPaginatedByCategory('');

        $this->assertContains($feedWithoutCategory->guid, array_pluck($pagination->items(), 'guid'));
        $this->assertNotContains($feedWithCategory->guid, array_pluck($pagination->items(), 'guid'));
    }

    public function testFindPaginatedByCategoryWithCategory(): void
    {
        $other = factory(Feed::class)->create();
        $expectedFeed = factory(Feed::class)->create(['category' => 'Symfony']);
        $pagination = $this->repository->findPaginatedByCategory('Symfony');

        $this->assertEquals(1, count($pagination->items()));
        $this->assertContains($expectedFeed->guid, array_pluck($pagination->items(), 'guid'));
        $this->assertNotContains($other->guid, array_pluck($pagination->items(), 'guid'));
    }

    public function testFindPaginatedByCategoryWithCustomPageParameters(): void
    {
        factory(Feed::class, 3)->create();
        $pagination = $this->repository->findPaginatedByCategory(null, 2, 2);

        $this->assertEquals(1, count($pagination->items()));
        $this->assertEquals(2, $pagination->currentPage());
    }

    public function testFindPaginatedByCategoryWithNonExistingPage(): void
    {
        factory(Feed::class)->create();
        $pagination = $this->repository->findPaginatedByCategory(null, 80);

        $this->assertTrue($pagination->isEmpty());
        $this->assertEquals(1, $pagination->total());
        $this->assertEquals(80, $pagination->currentPage());
    }

    public function testFindPaginatedByCategoryWithNegativeOrZeroPageParameters(): void
    {
        $pagination = $this->repository->findPaginatedByCategory(null, -66, 0);

        $this->assertEquals(1, $pagination->perPage());
        $this->assertEquals(1, $pagination->currentPage());
    }

    public function testExistsByGuidWhenNotExists(): void
    {
        $exists = $this->repository->existsByGuid('S-9dta9f');
        $this->assertFalse($exists);
    }

    public function testExistsByGuidWhenExists(): void
    {
        $feed = factory(Feed::class)->create();
        $exists = $this->repository->existsByGuid($feed->guid);
        $this->assertTrue($exists);
    }

    public function testCreate(): void
    {
        $feed = factory(Feed::class)->make();
        $this->repository->create($feed->getAttributes());

        $this->assertDatabaseHas('feeds', $feed->getAttributes());
    }
}