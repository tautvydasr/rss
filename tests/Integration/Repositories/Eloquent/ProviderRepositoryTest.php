<?php

namespace Tests\Integration\Repositories\Eloquent;

use App\Models\Feed;
use App\Models\Provider;
use App\Repositories\Eloquent\ProviderRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ProviderRepositoryTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * @var ProviderRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(ProviderRepository::class);
    }

    public function testFindNullReturned(): void
    {
        $result = $this->repository->find(88);
        $this->assertNull($result);
    }

    public function testFindModelReturned(): void
    {
        $provider = factory(Provider::class)->create();
        $result = $this->repository->find($provider->id);
        $this->assertEquals($provider->id, $result->id);
    }

    public function testFindAll(): void
    {
        $expectedProviders = factory(Provider::class, 3)->create();
        $providers = $this->repository->findAll();

        $this->assertEquals(3, $providers->count());
        $expectedProviders->each(function (Provider $expected) use ($providers) {
            $this->assertContains($expected->title, $providers->pluck('title'));
            $this->assertContains($expected->url, $providers->pluck('url'));
        });
    }

    public function testCreate(): void
    {
        $attributes = ['title' => 'Heisenberg', 'url' => 'https://www.crystal.org'];
        $this->repository->create($attributes);

        $this->assertDatabaseHas('providers', $attributes);
    }

    public function testUpdate(): void
    {
        $providerToUpdate = factory(Provider::class)->create(['title' => 'West world']);
        $this->repository->update($providerToUpdate, ['title' => 'East world']);

        $this->assertDatabaseHas('providers', ['title' => 'East world']);
        $this->assertDatabaseMissing('providers', ['title' => 'West world']);
    }

    public function testDelete(): void
    {
        $providerToDelete = factory(Provider::class)->create(['title' => 'Car spring']);
        $relatedFeed = factory(Feed::class)->create(['provider_id' => $providerToDelete->id]);
        $this->repository->delete($providerToDelete);

        $this->assertDatabaseMissing('providers', ['title' => 'Car spring']);
        $this->assertDatabaseMissing('feeds', ['id' => $relatedFeed->id]);
    }
}