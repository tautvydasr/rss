<?php

namespace Tests\Integration\Repositories\Eloquent;

use App\Models\Category;
use App\Models\Provider;
use App\Repositories\Eloquent\CategoryRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class CategoryRepositoryTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * @var CategoryRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(CategoryRepository::class);
    }

    public function testFindAll(): void
    {
        $expectedCategories = factory(Category::class, 2)->create();
        $categories = $this->repository->findAll();

        $this->assertEquals(2, $categories->count());
        $expectedCategories->each(function (Category $expected) use ($categories) {
            $this->assertContains($expected->title, $categories->pluck('title'));
        });
    }

    public function testCreate(): void
    {
        $attributes = ['title' => 'Pop-culture'];
        $this->repository->create($attributes);

        $this->assertDatabaseHas('categories', $attributes);
    }

    public function testUpdate(): void
    {
        $categoryToUpdate = factory(Category::class)->create(['title' => 'Sports']);
        $this->repository->update($categoryToUpdate, ['title' => 'E-sports']);

        $this->assertDatabaseHas('categories', ['title' => 'E-sports']);
        $this->assertDatabaseMissing('categories', ['title' => 'Sports']);
    }

    public function testDelete(): void
    {
        $categoryToDelete = factory(Category::class)->create();
        $relatedProvider = factory(Provider::class)->create(['category_id' => $categoryToDelete->id]);
        $this->repository->delete($categoryToDelete);

        $this->assertDatabaseMissing('categories', ['title' => $categoryToDelete->title]);
        $this->assertDatabaseHas('providers', ['title' => $relatedProvider->title, 'category_id' => null]);
    }
}