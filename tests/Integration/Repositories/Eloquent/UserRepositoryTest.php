<?php

namespace Tests\Integration\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\Eloquent\UserRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserRepositoryTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    /**
     * @var UserRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->app->make(UserRepository::class);
    }

    public function testCreate(): void
    {
        $attributes = ['email' => 'admin@rss.lt', 'role' => User::ROLE_ADMIN];
        $this->repository->create(array_merge($attributes, ['password' => 'secret']));

        $this->assertDatabaseHas('users', $attributes);
        $this->assertDatabaseMissing('users', ['password' => 'secret']);
    }

    public function testUpdate(): void
    {
        $userToUpdate = factory(User::class)->create(['email' => 'admin@rss.lt']);
        $this->repository->update($userToUpdate, ['email' => 'updated@rss.lt']);

        $this->assertDatabaseHas('users', ['email' => 'updated@rss.lt']);
        $this->assertDatabaseMissing('users', ['email' => 'admin@rss.lt']);
    }
}