<?php

namespace Tests\Unit\Services\Rss;

use App\Exceptions\InvalidModelException;
use App\Models\Provider;
use App\Repositories\Contracts\FeedRepository;
use App\Services\Rss\FeedLoader;
use App\Services\Rss\FeedParser;
use App\Services\Validators\FeedValidator;
use Prophecy\Argument;
use Tests\TestCase;

class FeedLoaderTest extends TestCase
{
    /**
     * @var FeedParser
     */
    private $parser;

    /**
     * @var FeedRepository
     */
    private $feeds;

    /**
     * @var FeedValidator
     */
    private $validator;

    /**
     * @var FeedLoader
     */
    private $loader;

    protected function setUp(): void
    {
        parent::setUp();

        $this->parser = $this->prophesize(FeedParser::class);
        $this->feeds = $this->prophesize(FeedRepository::class);
        $this->validator = $this->prophesize(FeedValidator::class);
        $this->loader = new FeedLoader($this->parser->reveal(), $this->feeds->reveal(), $this->validator->reveal());
    }

    public function testThrowExceptionOnFailedValidation(): void
    {
        $feed = collect([collect(['guid' => 'S-65835'])]);
        $this->parser->getFeed(Argument::type(Provider::class))->willReturn($feed);
        $this->feeds->existsByGuid('S-65835')->willReturn(false);
        $this->validator->validate(['guid' => 'S-65835'])->willReturn(collect(['title' => 'Required error.']));

        $this->expectException(InvalidModelException::class);
        $this->loader->loadFromProvider(new Provider());
    }

    public function testExistingFeedsFoundByGuidIsSkipped(): void
    {
        $feed = collect([collect(['guid' => 'hfPL8nhyEC'])]);
        $this->parser->getFeed(Argument::type(Provider::class))->willReturn($feed);
        $this->feeds->existsByGuid('hfPL8nhyEC')->willReturn(true);
        $this->validator->validate(Argument::any())->shouldNotBeCalled();
        $this->feeds->create(Argument::any())->shouldNotBeCalled();

        $this->loader->loadFromProvider(new Provider());
    }

    public function testSaveValidFeeds(): void
    {
        $feed = collect([collect(['guid' => 'F-1']), collect(['guid' => 'F-2'])]);
        $this->parser->getFeed(Argument::type(Provider::class))->willReturn($feed);

        foreach ($feed as $rss) {
            $this->feeds->existsByGuid($rss->get('guid'))->shouldBeCalled()->willReturn(false);
            $this->validator->validate($rss->all())->shouldBeCalled()->willReturn(collect());
            $this->feeds->create($rss->all())->shouldBeCalled();
        }

        $this->loader->loadFromProvider(new Provider());
    }
}