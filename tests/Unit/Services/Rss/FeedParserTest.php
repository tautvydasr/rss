<?php

namespace Tests\Unit\Services\Rss;

use App\Models\Provider;
use App\Services\Rss\FeedParser;
use App\Services\Rss\ItemParser;
use DOMElement;
use Exception;
use Prophecy\Argument;
use Tests\TestCase;

class FeedParserTest extends TestCase
{
    const EXAMPLE_XML = __DIR__ . DIRECTORY_SEPARATOR . 'example.xml';

    /**
     * @var ItemParser
     */
    private $parser;

    /**
     * @var FeedParser
     */
    private $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->parser = $this->prophesize(ItemParser::class);
        $this->service = new FeedParser($this->parser->reveal());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if (is_file(self::EXAMPLE_XML)) {
            unlink(self::EXAMPLE_XML);
        }
    }

    public function testXmlLoadFailed(): void
    {
        $this->expectException(Exception::class);
        $this->service->getFeed(new Provider(['url' => 'http://invalid.dev']));
    }

    public function testGetEmptyFeedFromInvalidXml(): void
    {
        file_put_contents(self::EXAMPLE_XML, <<<EOD
            <rss xmlns:content="http://purl.org/rss/1.0/modules/content/" version="2.0">
              <channel>
              </channel>
          </rss>
EOD
        );

        $feed = $this->service->getFeed(new Provider(['url' => self::EXAMPLE_XML]));
        $this->assertEmpty($feed);
    }

    public function testGetFeedFromXml(): void
    {
        file_put_contents(self::EXAMPLE_XML, <<<EOD
            <rss xmlns:content="http://purl.org/rss/1.0/modules/content/" version="2.0">
              <channel>
                <item>
                  <title>T1</title>
                </item>
            </channel>
            </rss>
EOD
        );

        $provider = new Provider();
        $provider->id = 1;
        $provider->url = self::EXAMPLE_XML;
        $this->parser->getAttributes(Argument::type(DOMElement::class))->willReturn(collect(['title' => 'T1']));

        $feed = $this->service->getFeed($provider);
        $this->assertContains('T1', $feed->pluck('title'));
        $this->assertContains(1, $feed->pluck('provider_id'));
    }
}