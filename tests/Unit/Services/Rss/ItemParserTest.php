<?php

namespace Tests\Unit\Services\Rss;

use App\Repositories\Contracts\FeedRepository;
use App\Services\Rss\ItemParser;
use Prophecy\Argument;
use Tests\TestCase;

class ItemParserTest extends TestCase
{
    /**
     * @var \DOMDocument
     */
    private $document;

    /**
     * @var FeedRepository
     */
    private $feeds;

    /**
     * @var ItemParser
     */
    private $parser;

    protected function setUp(): void
    {
        parent::setUp();

        $this->document = new \DOMDocument();
        $this->feeds = $this->prophesize(FeedRepository::class);
        $this->parser = new ItemParser($this->feeds->reveal());
    }

    public function testCreateGuidIfNotExists(): void
    {
        $element = $this->document->createElement('item');
        $this->feeds->existsByGuid(Argument::type('string'))->shouldBeCalled();

        $attributes = $this->parser->getAttributes($element);
        $this->assertArrayHasKey('guid', $attributes);
    }

    public function testExistingGuidShouldNotChange(): void
    {
        $element = $this->document->createElement('item');
        $element->appendChild($this->document->createElement('guid', 'S-vr8eaGrf9'));
        $this->feeds->existsByGuid(Argument::type('string'))->shouldNotBeCalled();

        $attributes = $this->parser->getAttributes($element);
        $this->assertEquals('S-vr8eaGrf9', $attributes->get('guid'));
    }

    public function testEmptyPublishDateFieldShouldNotBeFormatted(): void
    {
        $element = $this->document->createElement('item');
        $element->appendChild($this->document->createElement('guid', 'S-vr8eaGrf9'));
        $element->appendChild($this->document->createElement('pubDate', null));

        $attributes = $this->parser->getAttributes($element);
        $this->assertEmpty($attributes->get('published_at'));
    }

    public function testPublishDateFieldShouldBeFormatted(): void
    {
        $element = $this->document->createElement('item');
        $element->appendChild($this->document->createElement('guid', 'S-vr8eaGrf9'));
        $element->appendChild($this->document->createElement('dc:date', 'Wed, 27 Dec 2017 11:18:20 +0200'));

        $attributes = $this->parser->getAttributes($element);
        $this->assertEquals('2017-12-27 11:18:20', $attributes->get('published_at'));
    }

    public function testNonAcceptedNodesShouldBeStriped(): void
    {
        $element = $this->document->createElement('item');
        $element->appendChild($this->document->createElement('guid', 'S-vr8eaGrf9'));
        $element->appendChild($this->document->createElement('non-existing', 'value'));

        $attributes = $this->parser->getAttributes($element);
        $this->assertArrayNotHasKey('non-existing', $attributes);
    }

    public function testCleanAttributes(): void
    {
        $element = $this->document->createElement('item');
        $element->appendChild($this->document->createElement('guid', '          S-vr8eaGrf9'));
        $element->appendChild($this->document->createElement('description', 'My <b>bold</b>'));

        $attributes = $this->parser->getAttributes($element);
        $this->assertEquals('S-vr8eaGrf9', $attributes->get('guid'));
        $this->assertEquals('My bold', $attributes->get('description'));
    }

    public function testGetAttributes(): void
    {
        $element = $this->document->createElement('item');
        $element->appendChild($this->document->createElement('guid', 'S-vr8eaGrf9'));
        $element->appendChild($this->document->createElement('title', 'G8 meet'));
        $element->appendChild($this->document->createElement('link', 'www.g8.com'));
        $element->appendChild($this->document->createElement('description', null));
        $element->appendChild($this->document->createElement('dc:date', null));

        $attributes = $this->parser->getAttributes($element);
        $this->assertEquals('S-vr8eaGrf9', $attributes->get('guid'));
        $this->assertEquals('G8 meet', $attributes->get('title'));
        $this->assertEquals('www.g8.com', $attributes->get('link'));
        $this->assertEmpty($attributes->get('description'));
        $this->assertEmpty($attributes->get('published_at'));
    }
}