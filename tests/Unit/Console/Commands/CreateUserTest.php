<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\CreateUser;
use App\Models\User;
use App\Repositories\Contracts\UserRepository;
use App\Services\Validators\UserValidator;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    /**
     * @var UserRepository
     */
    private $users;

    /**
     * @var UserValidator
     */
    private $validator;

    /**
     * @var CommandTester
     */
    private $tester;

    protected function setUp(): void
    {
        parent::setUp();

        $this->users = $this->prophesize(UserRepository::class);
        $this->validator = $this->prophesize(UserValidator::class);

        $command = new CreateUser($this->users->reveal(), $this->validator->reveal());
        $command->setLaravel(app());
        $this->tester = new CommandTester($command);
    }

    public function testOutputValidationErrors(): void
    {
        $expectedError = 'The email already exists';
        $this->validator->validate($this->defaultAttributes())->shouldBeCalled()->willReturn(collect($expectedError));
        $this->users->create($this->defaultAttributes())->shouldNotBeCalled();

        $this->tester->execute(['--no-prompts' => null]);
        $output = $this->tester->getDisplay();

        $this->assertContains('Unable to create admin user', $output);
        $this->assertContains($expectedError, $output);
    }

    public function testAdminUserShouldBeCreated(): void
    {
        $this->validator->validate($this->defaultAttributes())->shouldBeCalled()->willReturn(collect());
        $this->users->create($this->defaultAttributes())->shouldBeCalled();

        $this->tester->execute(['--no-prompts' => null]);
        $this->assertContains('Admin user successfully created', $this->tester->getDisplay());
    }

    private function defaultAttributes(): array
    {
        return [
            'role' => User::ROLE_ADMIN,
            'email' => CreateUser::DEFAULT_EMAIL,
            'password' => CreateUser::DEFAULT_PASSWORD,
            'password_confirmation' => CreateUser::DEFAULT_PASSWORD,
        ];
    }
}