<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\UpdateFeed;
use App\Jobs\LoadRssFeed;
use App\Models\Provider;
use App\Repositories\Contracts\ProviderRepository;
use Illuminate\Support\Facades\Queue;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\TestCase;

class UpdateFeedTest extends TestCase
{
    /**
     * @var ProviderRepository
     */
    private $providers;

    /**
     * @var CommandTester
     */
    private $tester;

    protected function setUp(): void
    {
        parent::setUp();

        Queue::fake();
        $this->providers = $this->prophesize(ProviderRepository::class);
        $command = new UpdateFeed($this->providers->reveal());
        $command->setLaravel(app());
        $this->tester = new CommandTester($command);
    }

    public function testLoadJobNotPushedWithNotFoundProvider(): void
    {
        $this->providers->find(8)->willReturn(null);
        $this->tester->execute(['provider' => 8]);

        Queue::assertNotPushed(LoadRssFeed::class);
        $this->assertContains('[ERROR] Provider not found by id: 8', $this->tester->getDisplay());
    }

    public function testLoadJobNotPushedWithEmptyProviders(): void
    {
        $this->providers->findAll()->willReturn(collect());
        $this->tester->execute([]);

        Queue::assertNotPushed(LoadRssFeed::class);
        $this->assertEmpty($this->tester->getDisplay());
    }

    public function testLoadJobPushedWithSingleProvider(): void
    {
        $provider = new Provider(['title' => 'News from 17min.lt']);
        $this->providers->find(10)->willReturn($provider);
        $this->tester->execute(['provider' => 10]);

        Queue::assertPushed(LoadRssFeed::class, 1);
        $this->assertContains('[OK] Provider `News from 17min.lt` pushed to queue', $this->tester->getDisplay());
    }

    public function testLoadJobPushedWithAllProviders(): void
    {
        $this->providers->findAll()->willReturn(collect([
            new Provider(['title' => 'Fake news']),
            new Provider(['title' => 'Real news']),
        ]));
        $this->tester->execute([]);

        Queue::assertPushed(LoadRssFeed::class, 2);
        $this->assertContains('[OK] Provider `Fake news` pushed to queue', $this->tester->getDisplay());
        $this->assertContains('[OK] Provider `Real news` pushed to queue', $this->tester->getDisplay());
    }
}