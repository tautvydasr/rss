<?php

namespace Tests\Unit\Jobs;

use App\Exceptions\InvalidModelException;
use App\Jobs\LoadRssFeed;
use App\Models\Provider;
use App\Services\Rss\FeedLoader;
use Illuminate\Database\ConnectionInterface;
use Prophecy\Argument;
use Tests\TestCase;

class LoadRssFeedTest extends TestCase
{
    /**
     * @var ConnectionInterface
     */
    private $connection;

    /**
     * @var FeedLoader
     */
    private $loader;

    /**
     * @var LoadRssFeed
     */
    private $job;

    protected function setUp(): void
    {
        parent::setUp();

        $this->connection = $this->prophesize(ConnectionInterface::class);
        $this->loader = $this->prophesize(FeedLoader::class);
        $this->job = new LoadRssFeed(new Provider());
    }

    public function testSuccessfullyLoadFeedFromProvider(): void
    {
        $this->connection->beginTransaction()->shouldBeCalled();
        $this->loader->loadFromProvider(Argument::type(Provider::class))->shouldBeCalled();
        $this->connection->commit()->shouldBeCalled();

        $this->job->handle($this->connection->reveal(), $this->loader->reveal());
    }

    public function testFailedLoadFeedFromProvider(): void
    {
        $this->connection->beginTransaction()->shouldBeCalled();
        $this->loader->loadFromProvider(Argument::type(Provider::class))->willThrow(InvalidModelException::class);
        $this->connection->rollBack()->shouldBeCalled();

        $this->job->handle($this->connection->reveal(), $this->loader->reveal());
    }
}