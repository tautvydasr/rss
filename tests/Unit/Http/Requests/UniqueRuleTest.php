<?php

namespace Tests\Unit\Http\Requests;

use App\Http\Requests\UniqueRule;
use Tests\TestCase;

class UniqueRuleTest extends TestCase
{
    use UniqueRule;

    /**
     * @var object
     */
    private $model;

    public function testUniqueColumnIgnoringSelfWithoutModel(): void
    {
        $this->model = null;
        $rule = (string)$this->uniqueColumnIgnoringSelf('models', 'col', 'model');
        $this->assertEquals('unique:models,col,NULL,id', $rule);
    }

    public function testUniqueColumnIgnoringSelfWithModel(): void
    {
        $this->model = new class() {
            public $id = 2;
        };
        $rule = (string)$this->uniqueColumnIgnoringSelf('models', 'col', 'model');
        $this->assertEquals('unique:models,col,"2",id', $rule);
    }

    protected function route(): ?object
    {
        return $this->model;
    }
}