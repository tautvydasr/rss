<?php

namespace Tests\Unit\Http\Requests;

use App\Http\Requests\AuthorizeAdmin;
use App\Models\User;
use Tests\TestCase;

class AuthorizeAdminTest extends TestCase
{
    use AuthorizeAdmin;

    /**
     * @var User
     */
    private $user;

    public function testAuthorizeWithGuestUser(): void
    {
        $this->user = null;
        $this->assertFalse($this->authorize());
    }

    public function testAuthorizeWithNonAdminUser(): void
    {
        $this->user = new User(['role' => 'non-admin']);
        $this->assertFalse($this->authorize());
    }

    public function testAuthorizeWithAdminUser(): void
    {
        $this->user = new User(['role' => User::ROLE_ADMIN]);
        $this->assertTrue($this->authorize());
    }

    protected function user(): ?User
    {
        return $this->user;
    }
}