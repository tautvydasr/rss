<?php

namespace Tests\Unit\Http\Middleware;

use App\Http\Middleware\Admin;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Tests\TestCase;

class AdminTest extends TestCase
{
    /**
     * @var Guard
     */
    private $guard;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var \Closure
     */
    private $next;

    /**
     * @var Admin
     */
    private $middleware;

    protected function setUp(): void
    {
        parent::setUp();

        $this->guard = $this->prophesize(Guard::class);
        $this->request = $this->prophesize(Request::class);
        $this->next = function () { return true; };
        $this->middleware = new Admin($this->guard->reveal());
    }

    public function testThrowAuthenticationExceptionForGuests(): void
    {
        $this->guard->check()->willReturn(false);
        $this->expectException(AuthenticationException::class);
        $this->middleware->handle($this->request->reveal(), $this->next);
    }

    public function testThrowAuthenticationExceptionForNonAdmins(): void
    {
        $this->guard->check()->willReturn(true);
        $this->guard->user()->willReturn(factory(User::class)->make(['role' => 'non-admin']));
        $this->expectException(AuthenticationException::class);
        $this->middleware->handle($this->request->reveal(), $this->next);
    }

    public function testProceedRequestForAdmins(): void
    {
        $this->guard->check()->willReturn(true);
        $this->guard->user()->willReturn(factory(User::class)->make());
        $result = $this->middleware->handle($this->request->reveal(), $this->next);
        $this->assertTrue($result);
    }
}