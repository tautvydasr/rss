<?php

namespace Tests\Unit\Http\Middleware;

use App\Http\Middleware\RedirectIfAuthenticated;
use Illuminate\Auth\TokenGuard;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Tests\TestCase;

class RedirectIfAuthenticatedTest extends TestCase
{
    /**
     * @var Auth
     */
    private $auth;

    /**
     * @var Redirector
     */
    private $redirector;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var \Closure
     */
    private $next;

    /**
     * @var RedirectResponse
     */
    private $response;

    /**
     * @var RedirectIfAuthenticated
     */
    private $middleware;

    protected function setUp(): void
    {
        parent::setUp();

        $this->auth = $this->prophesize(Auth::class);
        $this->redirector = $this->prophesize(Redirector::class);
        $this->request = $this->prophesize(Request::class);
        $this->next = function () { return true; };
        $this->response = $this->prophesize(RedirectResponse::class)->reveal();
        $this->middleware = new RedirectIfAuthenticated($this->auth->reveal(), $this->redirector->reveal());
    }

    public function testRedirectAuthenticatedViaDefaultGuard(): void
    {
        $guard = $this->prophesize(Guard::class);
        $guard->check()->willReturn(true);
        $this->auth->guard(null)->shouldBeCalled()->willReturn($guard->reveal());
        $this->redirector->route('home')->shouldBeCalled()->willReturn($this->response);

        $result = $this->middleware->handle($this->request->reveal(), $this->next);
        $this->assertEquals($this->response, $result);
    }

    public function testRedirectAuthenticatedViaProvidedGuard(): void
    {
        $guard = $this->prophesize(TokenGuard::class);
        $guard->check()->willReturn(true);
        $this->auth->guard('api')->shouldBeCalled()->willReturn($guard->reveal());
        $this->redirector->route('home')->shouldBeCalled()->willReturn($this->response);

        $result = $this->middleware->handle($this->request->reveal(), $this->next, 'api');
        $this->assertEquals($this->response, $result);
    }

    public function testProceedRequestForGuests(): void
    {
        $guard = $this->prophesize(Guard::class);
        $guard->check()->willReturn(false);
        $this->auth->guard(null)->shouldBeCalled()->willReturn($guard->reveal());
        $this->redirector->route('home')->shouldNotBeCalled();

        $result = $this->middleware->handle($this->request->reveal(), $this->next);
        $this->assertTrue($result);
    }
}