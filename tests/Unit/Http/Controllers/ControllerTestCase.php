<?php

namespace Tests\Unit\Http\Controllers;

use Illuminate\Contracts\Translation\Translator;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Session\Store;
use Illuminate\View\View;
use Prophecy\Argument;
use Tests\TestCase;

class ControllerTestCase extends TestCase
{
    /**
     * @var ViewFactory
     */
    protected $views;

    /**
     * @var Store
     */
    protected $session;

    /**
     * @var Redirector
     */
    protected $redirector;

    /**
     * @var Translator
     */
    protected $translator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->views = $this->prophesize(ViewFactory::class);
        $this->session = $this->prophesize(Store::class);
        $this->redirector = $this->prophesize(Redirector::class);
        $this->translator = $this->prophesize(Translator::class);
    }

    protected function shouldBeRedirectedToRoute(string $route, array $parameters = []): void
    {
        $this->redirector->route($route, empty($parameters) ? Argument::any() : $parameters)
            ->shouldBeCalled()
            ->willReturn($this->prophesize(RedirectResponse::class)->reveal())
        ;
    }

    protected function shouldBeGeneratedView(string $view, array $parameters = []): void
    {
        $this->views->make($view, empty($parameters) ? Argument::any() : $parameters)
            ->shouldBeCalled()
            ->willReturn($this->prophesize(View::class)->reveal())
        ;
    }

    protected function shouldBeFlashedMessage(string $flash, string $model):void
    {
        $this->translator->trans("labels.model.$model")->shouldBeCalled()->willReturn($model);
        $this->translator->trans("messages.flash.$flash", ['model' => $model])->shouldBeCalled()->willReturn($flash);
        $this->session->flash('success', $flash)->shouldBeCalled();
    }
}