<?php

namespace Tests\Unit\Http\Controllers;

use App\Http\Controllers\CategoryController;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Repositories\Contracts\CategoryRepository;

class CategoryControllerTest extends ControllerTestCase
{
    /**
     * @var CategoryRepository
     */
    private $categories;

    /**
     * @var CategoryController
     */
    private $controller;

    protected function setUp(): void
    {
        parent::setUp();

        $this->categories = $this->prophesize(CategoryRepository::class);
        $this->controller = new CategoryController(
            $this->views->reveal(),
            $this->session->reveal(),
            $this->redirector->reveal(),
            $this->translator->reveal(),
            $this->categories->reveal()
        );
    }

    public function testIndex(): void
    {
        $this->categories->findAll()->shouldBeCalled()->willReturn(collect());
        $this->shouldBeGeneratedView('category.index', ['categories' => collect()]);
        $this->controller->index();
    }

    public function testCreate(): void
    {
        $this->shouldBeGeneratedView('category.create');
        $this->controller->create();
    }

    public function testStore(): void
    {
        $request = $this->prophesize(CategoryRequest::class);
        $request->validated()->willReturn(['title' => 'Hippogriff']);
        $this->categories->create(['title' => 'Hippogriff'])->shouldBeCalled();
        $this->shouldBeFlashedMessage('created', 'category');
        $this->shouldBeRedirectedToRoute('categories.index');

        $this->controller->store($request->reveal());
    }

    public function testEdit(): void
    {
        $category = $this->prophesize(Category::class);
        $this->shouldBeGeneratedView('category.edit', [
            'category' => $category->reveal(),
        ]);

        $this->controller->edit($category->reveal());
    }

    public function testUpdate(): void
    {
        $category = $this->prophesize(Category::class);
        $request = $this->prophesize(CategoryRequest::class);
        $request->validated()->willReturn(['title' => 'Hogwarts']);
        $this->categories->update($category->reveal(), ['title' => 'Hogwarts'])->shouldBeCalled();
        $this->shouldBeFlashedMessage('updated', 'category');
        $this->shouldBeRedirectedToRoute('categories.index');

        $this->controller->update($request->reveal(), $category->reveal());
    }

    public function testDestroy(): void
    {
        $category = $this->prophesize(Category::class);
        $this->categories->delete($category->reveal())->shouldBeCalled();
        $this->shouldBeFlashedMessage('deleted', 'category');
        $this->shouldBeRedirectedToRoute('categories.index');

        $this->controller->destroy($category->reveal());
    }
}