<?php

namespace Tests\Unit\Http\Controllers\Auth;

use App\Http\Controllers\Auth\ChangePasswordController;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\User;
use App\Repositories\Contracts\UserRepository;
use Tests\Unit\Http\Controllers\ControllerTestCase;

class ChangePasswordControllerTest extends ControllerTestCase
{
    /**
     * @var UserRepository
     */
    private $users;

    /**
     * @var ChangePasswordController
     */
    private $controller;

    protected function setUp(): void
    {
        parent::setUp();

        $this->users = $this->prophesize(UserRepository::class);
        $this->controller = new ChangePasswordController(
            $this->views->reveal(),
            $this->session->reveal(),
            $this->redirector->reveal(),
            $this->translator->reveal(),
            $this->users->reveal()
        );
    }

    public function testShowChangePasswordForm(): void
    {
        $this->shouldBeGeneratedView('auth.change');
        $this->controller->showChangePasswordForm();
    }

    public function testChangePassword(): void
    {
        $user = $this->prophesize(User::class);
        $request = $this->prophesize(ChangePasswordRequest::class);
        $request->validated()->willReturn(['password' => 'validated']);
        $request->user()->willReturn($user->reveal());

        $this->users->update($user->reveal(), ['password' => 'validated'])->shouldBeCalled();
        $this->shouldBeFlashedMessage('changed', 'password');
        $this->shouldBeRedirectedToRoute('feeds.index');

        $this->controller->changePassword($request->reveal());
    }
}