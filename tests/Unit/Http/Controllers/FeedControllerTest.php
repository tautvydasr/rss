<?php

namespace Tests\Unit\Http\Controllers;

use App\Http\Controllers\FeedController;
use App\Repositories\Contracts\FeedRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class FeedControllerTest extends ControllerTestCase
{
    /**
     * @var FeedRepository
     */
    private $feeds;

    /**
     * @var LengthAwarePaginator
     */
    private $paginator;

    /**
     * @var Request
     */
    private $request;
    /**
     * @var FeedController
     */
    private $controller;

    protected function setUp(): void
    {
        parent::setUp();

        $this->feeds = $this->prophesize(FeedRepository::class);
        $this->paginator = $this->prophesize(LengthAwarePaginator::class);
        $this->request = $this->prophesize(Request::class);
        $this->controller = new FeedController(
            $this->views->reveal(),
            $this->session->reveal(),
            $this->redirector->reveal(),
            $this->translator->reveal(),
            $this->feeds->reveal()
        );
    }

    public function testIndex(): void
    {
        $this->feeds->findPaginatedByCategory(null, 0)->shouldBeCalled()->willReturn($this->paginator->reveal());
        $this->feeds->findCategories()->shouldBeCalled()->willReturn(collect());
        $this->shouldBeGeneratedView('feed.index', [
            'feeds' => $this->paginator->reveal(),
            'categories' => collect(),
            'selectedCategory' => null,
        ]);
        $this->controller->index($this->request->reveal());
    }

    public function testIndexWithSelectedCategory(): void
    {
        $this->request->get('page')->willReturn(1);
        $this->request->has('category')->willReturn(true);
        $this->request->get('category')->willReturn('Alcatraz');
        $this->feeds->findPaginatedByCategory('Alcatraz', 1)->shouldBeCalled()->willReturn($this->paginator->reveal());
        $this->feeds->findCategories()->shouldBeCalled()->willReturn(collect());
        $this->shouldBeGeneratedView('feed.index', [
            'feeds' => $this->paginator->reveal(),
            'categories' => collect(),
            'selectedCategory' => 'Alcatraz',
        ]);
        $this->controller->index($this->request->reveal());
    }

    public function testIndexWithCategories(): void
    {
        $this->request->get('page')->willReturn(1);
        $this->request->has('category')->willReturn(false);
        $this->feeds->findPaginatedByCategory(null, 1)->shouldBeCalled()->willReturn($this->paginator->reveal());
        $this->feeds->findCategories()->shouldBeCalled()->willReturn(collect(['Crime', 'Pop']));
        $this->shouldBeGeneratedView('feed.index', [
            'feeds' => $this->paginator->reveal(),
            'categories' => collect(['Crime', 'Pop']),
            'selectedCategory' => null,
        ]);
        $this->controller->index($this->request->reveal());
    }
}