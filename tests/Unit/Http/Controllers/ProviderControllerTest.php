<?php

namespace Tests\Unit\Http\Controllers;

use App\Http\Controllers\ProviderController;
use App\Http\Requests\ProviderRequest;
use App\Models\Provider;
use App\Repositories\Contracts\CategoryRepository;
use App\Repositories\Contracts\ProviderRepository;

class ProviderControllerTest extends ControllerTestCase
{
    /**
     * @var ProviderRepository
     */
    private $providers;

    /**
     * @var CategoryRepository
     */
    private $categories;

    /**
     * @var ProviderController
     */
    private $controller;

    protected function setUp(): void
    {
        parent::setUp();

        $this->providers = $this->prophesize(ProviderRepository::class);
        $this->categories = $this->prophesize(CategoryRepository::class);
        $this->controller = new ProviderController(
            $this->views->reveal(),
            $this->session->reveal(),
            $this->redirector->reveal(),
            $this->translator->reveal(),
            $this->providers->reveal(),
            $this->categories->reveal()
        );
    }

    public function testIndex(): void
    {
        $this->providers->findGroupedByCategory()->shouldBeCalled()->willReturn(collect());
        $this->shouldBeGeneratedView('provider.index', ['groupedProviders' => collect()]);
        $this->controller->index();
    }

    public function testCreate(): void
    {
        $this->categories->findAll()->shouldBeCalled()->willReturn(collect([
            ['id' => 1, 'title' => 'Cat1', 'redundant' => 'redundant1'],
            ['id' => 2, 'title' => 'Cat2', 'redundant' => 'redundant2'],
        ]));
        $this->shouldBeGeneratedView('provider.create', [
            'categories' => collect([1 => 'Cat1', 2 => 'Cat2']),
        ]);

        $this->controller->create();
    }

    public function testStore(): void
    {
        $request = $this->prophesize(ProviderRequest::class);
        $request->validated()->willReturn(['title' => 'validated']);
        $this->providers->create(['title' => 'validated'])->shouldBeCalled();
        $this->shouldBeFlashedMessage('created', 'provider');
        $this->shouldBeRedirectedToRoute('providers.index');

        $this->controller->store($request->reveal());
    }

    public function testEdit(): void
    {
        $provider = $this->prophesize(Provider::class);
        $this->categories->findAll()->shouldBeCalled()->willReturn(collect());
        $this->shouldBeGeneratedView('provider.edit', [
            'provider' => $provider->reveal(),
            'categories' => collect(),
        ]);

        $this->controller->edit($provider->reveal());
    }

    public function testUpdate(): void
    {
        $provider = $this->prophesize(Provider::class);
        $request = $this->prophesize(ProviderRequest::class);
        $request->validated()->willReturn(['title' => 'edited']);
        $this->providers->update($provider->reveal(), ['title' => 'edited'])->shouldBeCalled();
        $this->shouldBeFlashedMessage('updated', 'provider');
        $this->shouldBeRedirectedToRoute('providers.index');

        $this->controller->update($request->reveal(), $provider->reveal());
    }

    public function testDestroy(): void
    {
        $provider = $this->prophesize(Provider::class);
        $this->providers->delete($provider->reveal())->shouldBeCalled();
        $this->shouldBeFlashedMessage('deleted', 'provider');
        $this->shouldBeRedirectedToRoute('providers.index');

        $this->controller->destroy($provider->reveal());
    }
}