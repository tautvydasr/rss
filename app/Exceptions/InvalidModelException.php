<?php

namespace App\Exceptions;

use Illuminate\Support\Collection;

class InvalidModelException extends \RuntimeException
{
    public static function failedValidation(Collection $attributes, Collection $errors): self
    {
        return new self(sprintf(
            'Model validation failed, attributes: `%s`, errors: `%s`',
            $attributes->toJson(),
            $errors->toJson()
        ));
    }
}