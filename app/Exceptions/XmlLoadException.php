<?php

namespace App\Exceptions;

class XmlLoadException extends \RuntimeException
{
    public static function fromUrl(string $url): self
    {
        return new self("Failed to load from provided url: $url");
    }
}