<?php

namespace App\Services\Validators;

use Illuminate\Contracts\Validation\Factory;
use Illuminate\Support\Collection;

abstract class ModelValidator
{
    /**
     * @var Factory
     */
    private $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public function validate(array $attributes): Collection
    {
        $validator = $this->factory->make($attributes, $this->rules());
        $errors = collect($validator->errors());

        return $errors->flatten();
    }

    abstract protected function rules(): array;
}