<?php

namespace App\Services\Validators;

use App\Models\User;

class UserValidator extends ModelValidator
{
    protected function rules(): array
    {
        return [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'role' => 'required|in:' . User::ROLE_ADMIN,
        ];
    }
}