<?php

namespace App\Services\Validators;

class FeedValidator extends ModelValidator
{
    protected function rules(): array
    {
        return [
            'guid' => 'required|unique:feeds,guid',
            'provider_id' => 'required|integer|exists:providers,id',
            'title' => 'required|string',
            'link' => 'required|url',
            'description' => 'string|nullable',
            'category' => 'string|nullable',
            'published_at' => 'required|date',
        ];
    }
}