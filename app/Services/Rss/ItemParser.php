<?php

namespace App\Services\Rss;

use App\Repositories\Contracts\FeedRepository;
use Carbon\Carbon;
use DOMElement;
use Illuminate\Support\Collection;

class ItemParser
{
    /**
     * @var FeedRepository
     */
    private $feeds;

    public function __construct(FeedRepository $feeds)
    {
        $this->feeds = $feeds;
    }

    public function getAttributes(DOMElement $element): Collection
    {
        $rss = collect($element->childNodes)->pluck('nodeValue', 'nodeName');
        $rss = $this->createGuid($rss);
        $rss = $this->resolvePublishDate($rss);
        $rss = $this->filterAcceptedNodes($rss);
        $rss = $this->cleanAttributes($rss);

        return $rss;
    }

    private function cleanAttributes(Collection $rss): Collection
    {
        return $rss->map(function ($attribute) {
            return trim(strip_tags($attribute));
        });
    }

    private function filterAcceptedNodes(Collection $rss): Collection
    {
        return $rss->only(['guid', 'title', 'link', 'description', 'category', 'published_at']);
    }

    private function resolvePublishDate(Collection $rss): Collection
    {
        $attribute = $rss->has('dc:date') ? 'dc:date' : 'pubDate';
        $published = $rss->pull($attribute);
        $published = $published ? Carbon::parse($published)->format('Y-m-d H:i:s') : null;

        return $rss->put('published_at', $published);
    }

    private function createGuid(Collection $rss): Collection
    {
        if ($rss->has('guid')) {
            return $rss;
        }

        do {
            $guid = str_random(10);
        } while ($this->feeds->existsByGuid($guid));

        return $rss->put('guid', $guid);
    }
}