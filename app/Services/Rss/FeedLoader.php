<?php

namespace App\Services\Rss;

use App\Exceptions\InvalidModelException;
use App\Models\Provider;
use App\Repositories\Contracts\FeedRepository;
use App\Services\Validators\FeedValidator;
use Illuminate\Support\Collection;

class FeedLoader
{
    /**
     * @var FeedParser
     */
    private $parser;

    /**
     * @var FeedRepository
     */
    private $feeds;

    /**
     * @var FeedValidator
     */
    private $validator;

    public function __construct(FeedParser $parser, FeedRepository $feeds, FeedValidator $validator)
    {
        $this->parser = $parser;
        $this->feeds = $feeds;
        $this->validator = $validator;
    }

    public function loadFromProvider(Provider $provider): void
    {
        foreach ($this->parser->getFeed($provider) as $rss) {
            if ($this->feeds->existsByGuid($rss->get('guid'))) {
                continue;
            }

            $this->validateFeed($rss);
            $this->feeds->create($rss->all());
        }
    }

    private function validateFeed(Collection $attributes): void
    {
        $errors = $this->validator->validate($attributes->all());

        if ($errors->isNotEmpty()) {
            throw InvalidModelException::failedValidation($attributes, $errors);
        }
    }
}