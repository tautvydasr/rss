<?php

namespace App\Services\Rss;

use App\Exceptions\XmlLoadException;
use App\Models\Provider;
use DOMDocument;
use Illuminate\Support\Collection;

class FeedParser
{
    /**
     * @var ItemParser
     */
    private $parser;

    public function __construct(ItemParser $parser)
    {
        $this->parser = $parser;
    }

    public function getFeed(Provider $provider): Collection
    {
        $feed = collect();
        $document = $this->loadXml($provider->url);

        foreach ($document->getElementsByTagName('item') as $domElement) {
            $parsedAttributes = $this->parser->getAttributes($domElement);
            $feed->push($parsedAttributes->put('provider_id', $provider->id));
        }

        return $feed;
    }

    private function loadXml(string $url): DOMDocument
    {
        $xml = new DOMDocument();

        if ($xml->load($url) === false) {
            throw XmlLoadException::fromUrl($url);
        }

        return $xml;
    }
}