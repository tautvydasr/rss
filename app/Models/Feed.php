<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 * @property int $provider_id
 * @property string $guid
 * @property string $title
 * @property string $link
 * @property string|null $category
 * @property string|null $description
 * @property \Carbon\Carbon $published_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Provider $provider
 */
class Feed extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'guid', 'provider_id', 'title', 'link', 'category', 'description', 'published_at',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'published_at',
    ];

    public function provider(): BelongsTo
    {
        return $this->belongsTo(Provider::class);
    }
}
