<?php

namespace App\Console\Commands;

use App\Jobs\LoadRssFeed;
use App\Repositories\Contracts\ProviderRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class UpdateFeed extends Command
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'feed:update {provider? : The ID of specific provider}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update feed from all existing providers';

    /**
     * @var ProviderRepository
     */
    private $providers;

    public function __construct(ProviderRepository $providers)
    {
        parent::__construct();
        $this->providers = $providers;
    }

    public function handle(): void
    {
        foreach ($this->getProviders() as $provider) {
            LoadRssFeed::dispatch($provider);
            $this->output->success("Provider `{$provider->title}` pushed to queue");
        }
    }

    private function getProviders(): Collection
    {
        if ($this->argument('provider') !== null) {
            if ($provider = $this->providers->find((int)$this->argument('provider'))) {
                return collect([$provider]);
            }

            $this->output->error("Provider not found by id: {$this->argument('provider')}");

            return collect();
        }

        return $this->providers->findAll();
    }
}