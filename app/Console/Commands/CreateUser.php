<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Repositories\Contracts\UserRepository;
use App\Services\Validators\UserValidator;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    const DEFAULT_EMAIL = 'admin@rss.dev';
    const DEFAULT_PASSWORD = 'secret';

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'user:create {--no-prompts : Create user with default values without asking user inputs}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new admin user';

    /**
     * @var UserRepository
     */
    private $users;

    /**
     * @var UserValidator
     */
    private $validator;

    public function __construct(UserRepository $users, UserValidator $validator)
    {
        parent::__construct();

        $this->users = $users;
        $this->validator = $validator;
    }

    public function handle(): void
    {
        $attributes = $this->getUserAttributes();
        $errors = $this->validator->validate($attributes);

        if ($errors->isEmpty()) {
            $this->users->create($attributes);
            $this->output->success('Admin user successfully created.');
        } else {
            $this->output->error('Unable to create admin user, try again');
            $this->output->title('Validation errors:');
            $this->output->listing($errors->all());
        }
    }

    private function getUserAttributes(): array
    {
        return [
            'role' => User::ROLE_ADMIN,
            'email' => $this->retrieveInput('What is the email?', self::DEFAULT_EMAIL),
            'password' => $this->retrieveHiddenInput('What is the password?', self::DEFAULT_PASSWORD),
            'password_confirmation' => $this->retrieveHiddenInput('Confirm the password?', self::DEFAULT_PASSWORD),
        ];
    }

    private function retrieveInput(string $question, string $default): string
    {
        if ($this->option('no-prompts')) {
            return $default;
        }

        return (string)$this->ask($question);
    }

    private function retrieveHiddenInput(string $question, string $default): string
    {
        if ($this->option('no-prompts')) {
            return $default;
        }

        return (string)$this->secret($question);
    }
}