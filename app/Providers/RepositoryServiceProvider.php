<?php

namespace App\Providers;

use App\Repositories\Contracts\CategoryRepository as CategoryRepositoryContract;
use App\Repositories\Contracts\FeedRepository as FeedRepositoryContract;
use App\Repositories\Contracts\ProviderRepository as ProviderRepositoryContract;
use App\Repositories\Contracts\UserRepository as UserRepositoryContract;
use App\Repositories\Eloquent\CategoryRepository as EloquentCategoryRepository;
use App\Repositories\Eloquent\FeedRepository as EloquentFeedRepository;
use App\Repositories\Eloquent\ProviderRepository as EloquentProviderRepository;
use App\Repositories\Eloquent\UserRepository as EloquentUserRepository ;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(UserRepositoryContract::class, EloquentUserRepository::class);
        $this->app->bind(ProviderRepositoryContract::class, EloquentProviderRepository::class);
        $this->app->bind(CategoryRepositoryContract::class, EloquentCategoryRepository::class);
        $this->app->bind(FeedRepositoryContract::class, EloquentFeedRepository::class);
    }
}