<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProviderRequest extends FormRequest
{
    use AuthorizeAdmin, UniqueRule;

    public function rules(): array
    {
        return [
            'title' => ['required', 'string', $this->uniqueColumnIgnoringSelf('providers', 'title', 'provider')],
            'url' => ['required', 'string', 'url', $this->uniqueColumnIgnoringSelf('providers', 'url', 'provider')],
            'category_id' => 'integer|nullable|exists:categories,id',
        ];
    }
}