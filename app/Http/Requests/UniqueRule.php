<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Unique;

trait UniqueRule
{
    protected function uniqueColumnIgnoringSelf(string $table, string $column, string $parameter): Unique
    {
        $model = $this->route($parameter);
        $ignore = $model ? $model->id : null;

        return Rule::unique($table, $column)->ignore($ignore);
    }
}