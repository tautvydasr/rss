<?php

namespace App\Http\Requests;

trait AuthorizeAdmin
{
    public function authorize(): bool
    {
        return $this->user() && $this->user()->isAdmin();
    }
}