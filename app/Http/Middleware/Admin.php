<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

class Admin
{
    /**
     * @var Guard
     */
    private $guard;

    public function __construct(Guard $guard)
    {
        $this->guard = $guard;
    }

    public function handle(Request $request, Closure $next)
    {
        if ($this->guard->check() && $this->guard->user()->isAdmin()) {
            return $next($request);
        }

        throw new AuthenticationException('Access denied for non-admin users.');
    }
}