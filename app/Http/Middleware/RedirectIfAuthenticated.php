<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class RedirectIfAuthenticated
{
    /**
     * @var Auth
     */
    private $auth;

    /**
     * @var Redirector
     */
    private $redirector;

    public function __construct(Auth $auth, Redirector $redirector)
    {
        $this->auth = $auth;
        $this->redirector = $redirector;
    }

    public function handle(Request $request, Closure $next, string $guard = null)
    {
        if ($this->auth->guard($guard)->check()) {
            return $this->redirector->route('home');
        }

        return $next($request);
    }
}
