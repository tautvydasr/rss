<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProviderRequest;
use App\Models\Provider;
use App\Repositories\Contracts\CategoryRepository;
use App\Repositories\Contracts\ProviderRepository;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Session\Store;
use Illuminate\View\View;

class ProviderController extends Controller
{
    /**
     * @var ProviderRepository
     */
    private $providers;

    /**
     * @var CategoryRepository
     */
    private $categories;

    public function __construct(
        ViewFactory $views,
        Store $session,
        Redirector $redirector,
        Translator $translator,
        ProviderRepository $providers,
        CategoryRepository $categories
    ) {
        parent::__construct($views, $session, $redirector, $translator);
        $this->providers = $providers;
        $this->categories = $categories;
    }

    public function index(): View
    {
        return $this->views->make('provider.index', [
            'groupedProviders' => $this->providers->findGroupedByCategory(),
        ]);
    }

    public function create(): View
    {
        return $this->views->make('provider.create', [
            'categories' => $this->categories->findAll()->pluck('title', 'id'),
        ]);
    }

    public function store(ProviderRequest $request): RedirectResponse
    {
        $this->providers->create($request->validated());
        $flash = $this->generateFlash('created', 'provider');

        return $this->redirectToRouteWithFlash('providers.index', $flash);
    }

    public function edit(Provider $provider): View
    {
        return $this->views->make('provider.edit', [
            'provider' => $provider,
            'categories' => $this->categories->findAll()->pluck('title', 'id'),
        ]);
    }

    public function update(ProviderRequest $request, Provider $provider): RedirectResponse
    {
        $this->providers->update($provider, $request->validated());
        $flash = $this->generateFlash('updated', 'provider');

        return $this->redirectToRouteWithFlash('providers.index', $flash);
    }

    public function destroy(Provider $provider): RedirectResponse
    {
        $this->providers->delete($provider);
        $flash = $this->generateFlash('deleted', 'provider');

        return $this->redirectToRouteWithFlash('providers.index', $flash);
    }
}
