<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Repositories\Contracts\CategoryRepository;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Session\Store;
use Illuminate\View\View;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $categories;

    public function __construct(
        ViewFactory $views,
        Store $session,
        Redirector $redirector,
        Translator $translator,
        CategoryRepository $categories
    ) {
        parent::__construct($views, $session, $redirector, $translator);
        $this->categories = $categories;
    }

    public function index(): View
    {
        return $this->views->make('category.index', [
            'categories' => $this->categories->findAll(),
        ]);
    }

    public function create(): View
    {
        return $this->views->make('category.create');
    }

    public function store(CategoryRequest $request): RedirectResponse
    {
        $this->categories->create($request->validated());
        $flash = $this->generateFlash('created', 'category');

        return $this->redirectToRouteWithFlash('categories.index', $flash);
    }

    public function edit(Category $category): View
    {
        return $this->views->make('category.edit', [
            'category' => $category,
        ]);
    }

    public function update(CategoryRequest $request, Category $category): RedirectResponse
    {
        $this->categories->update($category, $request->validated());
        $flash = $this->generateFlash('updated', 'category');

        return $this->redirectToRouteWithFlash('categories.index', $flash);
    }

    public function destroy(Category $category): RedirectResponse
    {
        $this->categories->delete($category);
        $flash = $this->generateFlash('deleted', 'category');

        return $this->redirectToRouteWithFlash('categories.index', $flash);
    }
}