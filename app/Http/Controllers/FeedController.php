<?php

namespace App\Http\Controllers;

use App\Repositories\Contracts\FeedRepository;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Session\Store;
use Illuminate\View\View;

class FeedController extends Controller
{
    /**
     * @var FeedRepository
     */
    private $feeds;

    public function __construct(
        ViewFactory $views,
        Store $session,
        Redirector $redirector,
        Translator $translator,
        FeedRepository $feeds
    ) {
        parent::__construct($views, $session, $redirector, $translator);
        $this->feeds = $feeds;
    }

    public function index(Request $request): View
    {
        $page = (int)$request->get('page');
        $category = $request->has('category') ? (string)$request->get('category') : null;

        return $this->views->make('feed.index', [
            'feeds' => $this->feeds->findPaginatedByCategory($category, $page),
            'categories' => $this->feeds->findCategories(),
            'selectedCategory' => $category,
        ]);
    }
}
