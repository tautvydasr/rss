<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Translation\Translator;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\Redirector;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Session\Store;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var ViewFactory
     */
    protected $views;

    /**
     * @var Store
     */
    private $session;

    /**
     * @var Redirector
     */
    protected $redirector;

    /**
     * @var Translator
     */
    protected $translator;

    public function __construct(ViewFactory $views, Store $session, Redirector $redirector, Translator $translator)
    {
        $this->views = $views;
        $this->session = $session;
        $this->redirector = $redirector;
        $this->translator = $translator;
    }

    protected function redirectToRouteWithFlash(string $route, string $message): RedirectResponse
    {
        $this->session->flash('success', $message);

        return $this->redirector->route($route);
    }

    protected function generateFlash(string $flash, string $model): string
    {
        return $this->translator->trans(
            "messages.flash.$flash",
            ['model' => $this->translator->trans("labels.model.$model")]
        );
    }
}
