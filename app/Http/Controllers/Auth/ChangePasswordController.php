<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Repositories\Contracts\UserRepository;
use Illuminate\Contracts\Translation\Translator;
use Illuminate\Contracts\View\Factory as ViewFactory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Session\Store;

class ChangePasswordController extends Controller
{
    /**
     * @var UserRepository
     */
    private $users;

    public function __construct(
        ViewFactory $views,
        Store $session,
        Redirector $redirector,
        Translator $translator,
        UserRepository $users
    ) {
        parent::__construct($views, $session, $redirector, $translator);
        $this->users = $users;
    }

    public function showChangePasswordForm(): View
    {
        return $this->views->make('auth.change');
    }

    public function changePassword(ChangePasswordRequest $request): RedirectResponse
    {
        $this->users->update($request->user(), $request->validated());
        $flash = $this->generateFlash('changed', 'password');

        return $this->redirectToRouteWithFlash('feeds.index', $flash);
    }
}