<?php

namespace App\Repositories\Eloquent;

use App\Models\Category;
use App\Repositories\Contracts\CategoryRepository as CategoryRepositoryContract;
use Illuminate\Support\Collection;

class CategoryRepository implements CategoryRepositoryContract
{
    /**
     * @var Category
     */
    private $model;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    public function findAll(): Collection
    {
        return $this->model->query()->with('providers')->get();
    }

    public function create(array $attributes): Category
    {
        return $this->model->query()->create($attributes);
    }

    public function update(Category $category, array $attributes): Category
    {
        $category->update($attributes);

        return $category;
    }

    public function delete(Category $category): void
    {
        $category->providers()->update(['category_id' => null]);
        $category->delete();
    }
}