<?php

namespace App\Repositories\Eloquent;

use App\Models\Feed;
use App\Repositories\Contracts\FeedRepository as FeedRepositoryContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class FeedRepository implements FeedRepositoryContract
{
    /**
     * @var Feed
     */
    private $model;

    public function __construct(Feed $model)
    {
        $this->model = $model;
    }

    public function findPaginatedByCategory(string $category = null, int $page = 1, int $perPage = 15): LengthAwarePaginator
    {
        $query = $this->model->query()->with('provider');
        $query->when($category !== null, function (Builder $q) use ($category) {
            if (empty($category)) {
                return $q->whereNull('category');
            }

            return $q->where('category', $category);
        });

        return $query->orderByDesc('published_at')->paginate(max(1, $perPage), ['*'], 'page', max(1, $page));
    }

    public function findCategories(): Collection
    {
        return $this->model->query()->select('category')->orderBy('category')->pluck('category')->unique();
    }

    public function existsByGuid(string $guid): bool
    {
        return $this->model->query()->where('guid', $guid)->exists();
    }

    public function create(array $attributes): Feed
    {
        return $this->model->query()->create($attributes);
    }
}