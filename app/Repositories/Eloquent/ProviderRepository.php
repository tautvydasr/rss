<?php

namespace App\Repositories\Eloquent;

use App\Models\Provider;
use App\Repositories\Contracts\ProviderRepository as ProviderRepositoryContract;
use Illuminate\Support\Collection;

class ProviderRepository implements ProviderRepositoryContract
{
    /**
     * @var Provider
     */
    private $model;

    public function __construct(Provider $model)
    {
        $this->model = $model;
    }

    public function find(int $id): ?Provider
    {
        return $this->model->query()->find($id);
    }

    public function findAll(): Collection
    {
        return $this->model->query()->get();
    }

    public function findGroupedByCategory(): Collection
    {
        return $this->model->query()
            ->with('category', 'feeds')
            ->orderByDesc('category_id')
            ->get()
            ->groupBy('category.title')
        ;
    }

    public function create(array $attributes): Provider
    {
        return $this->model->query()->create($attributes);
    }

    public function update(Provider $provider, array $attributes): Provider
    {
        $provider->update($attributes);

        return $provider;
    }

    public function delete(Provider $provider): void
    {
        $provider->feeds()->delete();
        $provider->delete();
    }
}