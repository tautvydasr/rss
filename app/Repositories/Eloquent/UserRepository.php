<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\Contracts\UserRepository as UserRepositoryContract;
use Illuminate\Contracts\Hashing\Hasher;

class UserRepository implements UserRepositoryContract
{
    /**
     * @var User
     */
    private $model;

    /**
     * @var Hasher
     */
    private $hasher;

    public function __construct(User $model, Hasher $hasher)
    {
        $this->model = $model;
        $this->hasher = $hasher;
    }

    public function create(array $attributes): User
    {
        return $this->model->query()->create($this->encryptPassword($attributes));
    }

    public function update(User $user, array $attributes): User
    {
        $user->update($this->encryptPassword($attributes));

        return $user;
    }

    private function encryptPassword(array $attributes): array
    {
        if (isset($attributes['password'])) {
            $encryptedPassword = $this->hasher->make($attributes['password']);
            $attributes = array_replace($attributes, ['password' => $encryptedPassword]);
        }

        return $attributes;
    }
}