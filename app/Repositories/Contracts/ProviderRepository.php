<?php

namespace App\Repositories\Contracts;

use App\Models\Provider;
use Illuminate\Support\Collection;

interface ProviderRepository
{
    public function find(int $id): ?Provider;
    public function findAll(): Collection;
    public function findGroupedByCategory(): Collection;
    public function create(array $attributes): Provider;
    public function update(Provider $provider, array $attributes): Provider;
    public function delete(Provider $provider): void;
}