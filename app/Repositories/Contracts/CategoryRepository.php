<?php

namespace App\Repositories\Contracts;

use App\Models\Category;
use Illuminate\Support\Collection;

interface CategoryRepository
{
    public function findAll(): Collection;
    public function create(array $attributes): Category;
    public function update(Category $category, array $attributes): Category;
    public function delete(Category $category): void;
}