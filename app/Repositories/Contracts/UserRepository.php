<?php

namespace App\Repositories\Contracts;

use App\Models\User;

interface UserRepository
{
    public function create(array $attributes): User;
    public function update(User $user, array $attributes): User;
}