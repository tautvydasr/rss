<?php

namespace App\Repositories\Contracts;

use App\Models\Feed;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface FeedRepository
{
    public function findPaginatedByCategory(string $category = null, int $page = 0, int $perPage = 15): LengthAwarePaginator;
    public function findCategories(): Collection;
    public function existsByGuid(string $guid): bool;
    public function create(array $attributes): Feed;
}