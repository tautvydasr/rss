<?php

namespace App\Jobs;

use App\Exceptions\InvalidModelException;
use App\Models\Provider;
use App\Services\Rss\FeedLoader;
use Illuminate\Bus\Queueable;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class LoadRssFeed implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    public $tries = 3;

    /**
     * @var Provider
     */
    private $provider;

    public function __construct(Provider $provider)
    {
        $this->provider = $provider;
    }

    public function handle(ConnectionInterface $connection, FeedLoader $loader): void
    {
        $connection->beginTransaction();

        try {
            $loader->loadFromProvider($this->provider);
            $connection->commit();
        } catch (InvalidModelException $e) {
            $connection->rollBack();
            $this->fail($e);
        }
    }
}
