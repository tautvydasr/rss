<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('feeds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guid');
            $table->unsignedInteger('provider_id');
            $table->string('title');
            $table->string('link');
            $table->string('category')->nullable();
            $table->text('description')->nullable();
            $table->dateTime('published_at');
            $table->timestamps();

            $table->foreign('provider_id')->references('id')->on('providers')->onDelete('cascade');
            $table->index('guid');
            $table->unique('guid');
            $table->index('provider_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('feeds');
    }
}
