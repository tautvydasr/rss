<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProvidersCategoriesRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        // Drop existing indexes in order to prevent name collision
        Schema::table('providers', function (Blueprint $table) {
            $table->dropUnique('providers_title_unique');
            $table->dropUnique('providers_url_unique');
        });

        // Because Sqlite alter table does not support add constraint, create tmp table with foreign key
        Schema::create('tmp_providers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')->nullable();
            $table->string('title');
            $table->string('url');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('SET NULL');
            $table->index('category_id', 'providers_category_id_index');

            $table->unique('title', 'providers_title_unique');
            $table->unique('url', 'providers_url_unique');
        });

        $this->copyExistingProviders();
        $this->switchTables();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        // Drop existing indexes in order to prevent name collision
        Schema::table('providers', function (Blueprint $table) {
            $table->dropUnique('providers_title_unique');
            $table->dropUnique('providers_url_unique');
            $table->dropIndex('providers_category_id_index');
        });

        // Because Sqlite alter table does not support drop column, create tmp table without column
        Schema::create('tmp_providers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('url');
            $table->timestamps();

            $table->unique('title', 'providers_title_unique');
            $table->unique('url', 'providers_url_unique');
        });

        $this->copyExistingProviders();
        $this->switchTables();
    }

    private function copyExistingProviders(): void
    {
        foreach (DB::table('providers')->get(['id', 'title', 'url', 'created_at', 'updated_at']) as $provider) {
            DB::table('tmp_providers')->insert((array)$provider);
        }
    }

    private function switchTables(): void
    {
        Schema::dropIfExists('providers');
        Schema::rename('tmp_providers', 'providers');
    }
}
