<?php

use App\Models\Feed;
use Illuminate\Database\Seeder;

class FeedsTableSeeder extends Seeder
{
    public function run(): void
    {
        factory(Feed::class, 15)->create();
    }
}