<?php

use App\Models\Provider;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class ProvidersTableSeeder extends Seeder
{
    public function run(): void
    {
        $this->providers()->each(function (string $url) {
            factory(Provider::class)->create(['url' => $url]);
        });
    }

    private function providers(): Collection
    {
        return collect([
            'http://www.feedforall.com/sample.xml',
            'http://feeds.feedburner.com/technologijos-visos-publikacijos?format=xml',
            'https://www.alfa.lt/rss',
        ]);
    }
}