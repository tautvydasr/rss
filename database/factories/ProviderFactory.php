<?php

use App\Models\Category;
use App\Models\Provider;
use Faker\Generator as Faker;

$factory->define(Provider::class, function (Faker $faker) {
    $category = Category::inRandomOrder()->limit(1)->get()->first();

    return [
        'title' => $faker->unique()->company,
        'url' => $faker->url,
        'category_id' => $faker->boolean && $category ? $category->id : null,
    ];
});
