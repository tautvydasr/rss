<?php

use App\Models\Feed;
use App\Models\Provider;
use Faker\Generator as Faker;

$factory->define(Feed::class, function (Faker $faker) {
    $provider = Provider::inRandomOrder()->limit(1)->get()->first();

    if ($provider === null) {
        $provider = factory(Provider::class)->create();
    }

    return [
        'provider_id' => $provider->id,
        'guid' => $faker->unique()->uuid,
        'title' => $faker->sentence($faker->numberBetween(1, 4)),
        'link' => $faker->url,
        'category' => $faker->boolean ? ucfirst($faker->word) : null,
        'description' => $faker->boolean ? $faker->text() : null,
        'published_at' => $faker->dateTimeThisYear(),
    ];
});