#!/bin/bash

WORK_DIR=/var/www

if [ -f "$WORK_DIR/composer.json" ]; then
    echo "[info] Running composer install"
    composer install --working-dir="$WORK_DIR"
fi

if [ ! -f "$WORK_DIR/.env" ] && [ -f "$WORK_DIR/.env.example" ]; then
    echo "[info] Generating .env file"
    cp "$WORK_DIR/.env.example" "$WORK_DIR/.env"
    php "$WORK_DIR/artisan" key:generate
fi

echo "[info] Setup database"
if [ -f "$WORK_DIR/database/local.sqlite" ]; then
    php "$WORK_DIR/artisan" migrate
else
    touch "$WORK_DIR/database/local.sqlite"
    chown -R www-data:www-data "$WORK_DIR/database/local.sqlite"
    php "$WORK_DIR/artisan" migrate:fresh --seed
fi

echo "[info] Change owner of www-data specific directories"
chown -R www-data:www-data "$WORK_DIR/storage"
chown -R www-data:www-data "$WORK_DIR/bootstrap/cache"

echo "[info] Starting php-fpm"
exec php-fpm
