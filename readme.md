Helis RSS
===========

This is a test exercise for backend developer position.

###Local application setup for development

To setup project you should have `docker` and `docker-compose` installed on your machine

- Copy example docker-compose file
```
cp docker-compose.yml.example docker-compose.yml
```
- Create docker containers (recommended first time to view setup steps)
```
docker-compose up
```
- Or add `-d` argument to run containers in background 
```
docker-compose up -d
```

###User system

In order to login you should create admin user via console command or use the one from seeded data (all seeded users have password `secret`)

- Connect to php container
```
docker exec -it rss_php_1 /bin/bash
```
- Run `user:create` command and follow further instructions
```
php artisan user:create
```

###Feed update

In order to update feed from providers on local machine you should set queues and run worker

- Connect to php container
```
docker exec -it rss_php_1 /bin/bash
```
- Set queues for all providers
```
php artisan feed:update 
```
- Or set queue for one specific provider by passing it's id as an argument (for example `2` in this case)
```
php artisan feed:update 2
```
- Run laravel queue worker (if something went wrong stop worker and re-run this command you do not need to setup queues they are on `jobs` table, failed jobs can be found on `failed_jobs` table)
```
php artisan queue:work 
```

###Testing
- Connect to php container
```
docker exec -it rss_php_1 /bin/bash
```
- Run tests with command
```
./vendor/bin/phpunit
```